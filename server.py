from aiohttp import web
import time

routes = web.RouteTableDef()


class RateLimiter:
    def __init__(self):
        self.last_requests = []
        self.window_duration = 60
        self.requests_per_window = 3

    def new_request(self):
        self.last_requests.append(time.time())
        self.last_requests = [
            ts for ts in self.last_requests
            if self.last_requests[-1] - ts < self.window_duration
        ]

    def is_limit_exceeded(self):
        return len(self.last_requests) > self.requests_per_window

    def set_limit(self, rpw):
        self.requests_per_window = int(rpw)

    def limit(self, fn):
        def inner(*args, **kwarg):
            self.new_request()
            if self.is_limit_exceeded():
                self.last_requests.pop()
                raise web.HTTPNotAcceptable()

            return fn(*args, **kwarg)
        return inner


rate_limiter = RateLimiter()


@routes.get('/')
@rate_limiter.limit
async def hello_dudes(request):
    return web.Response(text='Hello, dude!')


@routes.get('/set-rate')
async def set_rate(request):
    rate = request.query.get('rate')
    if rate:
        rate_limiter.set_limit(rate)
    resp = web.Response()
    resp.text = 'OK'
    return resp


app = web.Application()
app.router.add_routes(routes)
web.run_app(app)
