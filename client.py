import requests
import sys
import time


url, *_ = sys.argv[1:]

while True:
    resp = requests.get(url)
    print(resp.text)
    if resp.status_code == 200:
        time.sleep(1)
        continue
    else:
        time.sleep(5)
        continue


